---
title: "First Post"
date: 2019-01-02T12:12:31Z
draft: false
---
This is my first post on the site.  I hope that you like it!

## Welcome Function

Here is a little Python function to welcome you:

{{< highlight python >}}
def hello_world():
    print "Hello there!"
{{< /highlight >}}

![This is an image](/img/test_image.gif)
