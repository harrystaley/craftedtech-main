---
title: "Common Ports"
date: 2022-03-07T12:12:31Z
draft: false
---

This is a list of common ports for reference.

|Port|Protocol|
|-----|-------|
|20|FTP data (File Transfer Protocol)|
|21|FTP (File Transfer Protocol)|
|22|SSH (Secure Shell)|
|23|Telnet|
|25|SMTP (Send Mail Transfer Protocol)|
|43|whois|
|53|DNS (Domain Name Service)|
|68|DHCP (Dynamic Host Control Protocol)|
|79|Finger|
|80|HTTP (HyperText Transfer Protocol)|
|110|POP3 (Post Office Protocol, version 3)|
|115|SFTP (Secure File Transfer Protocol)|
|119|NNTP (Network New Transfer Protocol)|
|123|NTP (Network Time Protocol)|
|137|NetBIOS-ns|
|138|NetBIOS-dgm|
|139|NetBIOS|
|143|IMAP (Internet Message Access Protocol)|
|161|SNMP (Simple Network Management Protocol)|
|194|IRC (Internet Relay Chat)|
|220|IMAP3 (Internet Message Access Protocol 3)|
|389|LDAP (Lightweight Directory Access Protocol)|
|443|SSL (Secure Socket Layer)|
|445|SMB (NetBIOS over TCP)|
